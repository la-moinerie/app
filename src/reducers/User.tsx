import { UserActions } from '../actions/User';
import { IUser } from '../types';

const UserReducer = (state = null, action: UserActions) => {
  switch (action.type) {
    case 'GET_USERS_SUCCESS':
      return (action.payload as IUser[]) ? (action.payload as IUser[])[0] : state;
    default:
      return state;
  }
}

export default UserReducer;