import { SignInActions } from '../actions/SignIn';

const SignInReducer = (user = {}, action: SignInActions) => {
  switch (action.type) {
    case 'USER_NAME_CHANGE':
      return action.payload !== null ?
        {
          ...user,
          signInEmail: (action.payload as string)
        }
        : user;
    case 'USER_PASSWORD_CHANGE':
      return action.payload !== null ?
        {
          ...user,
          signInPassword: (action.payload as string)
        }
        : user;
    case 'SIGN_IN_SUCCESS':
      return action.payload !== null ?
        {
          ...user,
          email: (action.payload as string)
        }
        : user;
    default:
      return user;
  }
}

export default SignInReducer;