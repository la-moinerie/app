import { ReservationActions } from '../actions/Reservation';
import { IReservation } from '../types';

const ReservationsReducer = (reservations = [], action: ReservationActions) => {
  switch (action.type) {
    case 'GET_RESERVATIONS_SUCCESS':
      return (action.payload as IReservation[]) ? (action.payload as IReservation[]) : reservations;
    default:
      return reservations;
  }
}

export default ReservationsReducer;