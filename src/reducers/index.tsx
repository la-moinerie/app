import { combineReducers } from 'redux';
import ReservationsReducer from './Reservations';
import SignInReducer from './SignIn';

export default combineReducers({
  reservations: ReservationsReducer,
  user: SignInReducer,
})