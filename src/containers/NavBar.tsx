import { connect } from 'react-redux';

import NavBar from '../components/NavBar';
import IStoreState from '../types';

export function mapStateToProps({ user }: IStoreState) {
  return {
    user,
  };
}

export default connect(mapStateToProps)(NavBar);