import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import * as actions from '../actions/SignIn';
import SignIn from '../components/SignIn';
import IStoreState from '../types/';

export function mapStateToProps({ user }: IStoreState) {
  return {
    user,
  };
}

export function mapDispatchToProps(dispatch: Dispatch<actions.SignInActions>) {
  return {
    onSubmitClick: (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault();
      dispatch(actions.signIn());
    },
    onUserNameChange: (e: React.ChangeEvent<HTMLInputElement>) =>
      dispatch(actions.handleUserNameChange(e.target.value)),
    onUserPasswordChange: (e: React.ChangeEvent<HTMLInputElement>) =>
      dispatch(actions.handleUserPasswordChange(e.target.value)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);