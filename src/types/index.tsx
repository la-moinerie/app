import { RouterState } from 'connected-react-router';

export default interface IStoreState {

  reservations: IReservation[];
  user: IUser;
  router: RouterState;

}

export interface IReservation {

  end: Date;
  start: Date;

}

export interface IUser {

  email: string;
  signInEmail: string;
  signInPassword: string;

}