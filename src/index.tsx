import CssBaseline from '@material-ui/core/CssBaseline';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

// import { getSession } from './actions/SignIn';
import App from './App';
import { history, store } from './config';
import registerServiceWorker from './registerServiceWorker';

import './index.css';

const theme = createMuiTheme();

// store.dispatch(getSession());

ReactDOM.render(
  <Provider store={store}>
    <MuiThemeProvider theme={theme}>
      <React.Fragment>
        <CssBaseline />
        <App history={history} />
      </React.Fragment>
    </MuiThemeProvider>
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
