import axios from 'axios';
import { Dispatch } from 'redux';

import * as constants from '../constants/Reservation';
import { IReservation } from '../types';

export interface IReservationActions {
  type: constants.GET_RESERVATIONS_ATTEMPT
      | constants.GET_RESERVATIONS_SUCCESS
      | constants.GET_RESERVATIONS_ERROR
      | constants.SAVE_RESERVATION_ATTEMPT
      | constants.SAVE_RESERVATION_SUCCESS
      | constants.SAVE_RESERVATION_ERROR
      | constants.UPDATE_RESERVATION_ATTEMPT
      | constants.UPDATE_RESERVATION_SUCCESS
      | constants.UPDATE_RESERVATION_ERROR;
  payload?: IReservation | IReservation[];
  error?: any;
}

export function GetReservationsAttempt(): IReservationActions {
  return {
    type: constants.GET_RESERVATIONS_ATTEMPT
  };
}

export function GetReservationsSuccess(reservations: IReservation[]): IReservationActions {
  return {
    payload: reservations,
    type: constants.GET_RESERVATIONS_SUCCESS,
  };
}

export function GetReservationsError(err: any): IReservationActions {
  return {
    error: err,
    payload: [],
    type: constants.GET_RESERVATIONS_ERROR,
  };
}

export function getReservations(): any {
  return (dispatch: Dispatch<ReservationActions>) => {
    dispatch(GetReservationsAttempt());

    axios.get('http://' + location.hostname + ':3001/reservations')
      .then(res => {
        const reservations: IReservation[] = res.data;
        dispatch(GetReservationsSuccess(reservations));
      })
      .catch(err => {
        dispatch(GetReservationsError(err));
      });
  };
}

export function SaveReservationAttempt(): IReservationActions {
  return {
    type: constants.SAVE_RESERVATION_ATTEMPT
  };
}

export function SaveReservationSuccess(reservation: IReservation): IReservationActions {
  return {
    payload: reservation,
    type: constants.SAVE_RESERVATION_SUCCESS,
  };
}

export function SaveReservationError(err: any): IReservationActions {
  return {
    error: err,
    payload: [],
    type: constants.SAVE_RESERVATION_ERROR,
  };
}

export function saveReservation(reservation: IReservation): any {
  return (dispatch: Dispatch<ReservationActions>) => {
    dispatch(SaveReservationAttempt());

    axios.post('http://' + location.hostname + '/reservations', reservation)
      .then(res => {
        const savedReservation: IReservation = res.data;
        dispatch(SaveReservationSuccess(savedReservation));
      })
      .catch(err => {
        dispatch(SaveReservationError(err));
      });
  };
}

export function UpdateReservationAttempt(): IReservationActions {
  return {
    type: constants.UPDATE_RESERVATION_ATTEMPT
  };
}

export function UpdateReservationSuccess(reservation: IReservation): IReservationActions {
  return {
    payload: reservation,
    type: constants.UPDATE_RESERVATION_SUCCESS,
  };
}

export function UpdateReservationError(err: any): IReservationActions {
  return {
    error: err,
    payload: [],
    type: constants.UPDATE_RESERVATION_ERROR,
  };
}

export function updateReservation(reservation: IReservation): any {
  return (dispatch: Dispatch<ReservationActions>) => {
    dispatch(UpdateReservationAttempt());

    axios.patch('http://' + location.hostname + '/reservations', reservation)
      .then(res => {
        const savedReservation: IReservation = res.data;
        dispatch(UpdateReservationSuccess(savedReservation));
      })
      .catch(err => {
        dispatch(UpdateReservationError(err));
      });
  };
}

export type ReservationActions = IReservationActions;