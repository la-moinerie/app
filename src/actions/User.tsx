import axios from 'axios';
import { Dispatch } from 'redux';

import * as constants from '../constants/User';
import { IUser } from '../types';

export interface IUserActions {
  type: constants.GET_USERS_ATTEMPT
      | constants.GET_USERS_SUCCESS
      | constants.GET_USERS_ERROR;
  payload?: IUser[];
  error?: any;
}

export function GetUsersAttempt(): IUserActions {
  return {
    type: constants.GET_USERS_ATTEMPT
  };
}

export function GetUsersSuccess(users: IUser[]): IUserActions {
  return {
    payload: users,
    type: constants.GET_USERS_SUCCESS,
  };
}

export function GetUsersError(err: any): IUserActions {
  return {
    error: err,
    payload: [],
    type: constants.GET_USERS_ERROR,
  };
}

export function getUsers(): any {
  return (dispatch: Dispatch<UserActions>) => {
    dispatch(GetUsersAttempt());

    axios.get('http://' + location.hostname + ':3001/reservations')
      .then(res => {
        const users: IUser[] = res.data;
        dispatch(GetUsersSuccess(users));
      })
      .catch(err => {
        dispatch(GetUsersError(err));
      });
  };
}

export type UserActions = IUserActions;