import axios from 'axios';
import { Dispatch } from 'redux';

import * as constants from '../constants/SignIn';
import IStoreState, { IUser } from '../types';

export interface ISignInActions {
  type: constants.USER_NAME_CHANGE
      | constants.USER_PASSWORD_CHANGE
      | constants.SIGN_IN_ATTEMPT
      | constants.SIGN_IN_SUCCESS
      | constants.SIGN_IN_ERROR;
  payload?: string | IUser;
  error?: any;
}

export function handleUserNameChange(name: string): ISignInActions {
  return {
    payload: name,
    type: constants.USER_NAME_CHANGE,
  };
}

export function handleUserPasswordChange(name: string): ISignInActions {
  return {
    payload: name,
    type: constants.USER_PASSWORD_CHANGE,
  };
}

export function SignInAttempt(): ISignInActions {
  return {
    type: constants.SIGN_IN_ATTEMPT
  };
}

export function SignInSuccess(userEmail: string): ISignInActions {
  return {
    payload: userEmail,
    type: constants.SIGN_IN_SUCCESS,
  };
}

export function SignInError(err: any): ISignInActions {
  return {
    error: err,
    payload: undefined,
    type: constants.SIGN_IN_ERROR,
  };
}

export function signIn(): any {
  return (dispatch: Dispatch<SignInActions>, getState: () => IStoreState) => {
    const state: IStoreState = getState();
    const userDto: any = {email: state.user.signInEmail, password: state.user.signInPassword}

    dispatch(SignInAttempt());

    axios.post('http://' + location.hostname + ':3000/user/signIn', userDto)
      .then(res => {
        dispatch(SignInSuccess(res.data.email));
      })
      .catch(err => {
        dispatch(SignInError(err));
      });
  };
}

export function getSession(): any {
  return (dispatch: Dispatch<SignInActions>, getState: () => IStoreState) => {
    axios.get('http://' + location.hostname + ':3000/session/find')
      .then(res => {
        // tslint:disable-next-line:no-console
        console.log(res);
      })
      .catch(err => {
        // tslint:disable-next-line:no-console
        console.log(err);
      });
  };
}

export type SignInActions = ISignInActions;