import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import InputLabel from '@material-ui/core/InputLabel';
import AccountCircle from '@material-ui/icons/AccountCircle';
import VpnKeyRounded from '@material-ui/icons/VpnKeyRounded';
import * as React from 'react';

import { IUser } from '../types';

import './SignIn.css';

export interface IProps {
  onSubmitClick: (e: React.FormEvent<HTMLFormElement>) => void;
  onUserNameChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onUserPasswordChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  user: IUser;
}

function SignIn(props: IProps) {
  return (
    <div>
      <form noValidate={true}
            onSubmit={props.onSubmitClick}
            autoComplete="off">
        <FormControl
          className="Email"
          required={true}
          error={!props.user.signInEmail}>
          <InputLabel htmlFor="email">Email</InputLabel>
          <Input
            id="email"
            value={props.user.signInEmail}
            onChange={props.onUserNameChange}
            startAdornment={
              <InputAdornment position="start">
                <AccountCircle />
              </InputAdornment>
            }
          />
          <FormHelperText id="name-error-text">
            {!props.user.signInEmail ? 'Email is required' : ''}
          </FormHelperText>
        </FormControl>
        <FormControl
          className="Password"
          required={true}
          error={!props.user.signInPassword}>
          <InputLabel htmlFor="password">Password</InputLabel>
          <Input
            id="password"
            type="password"
            value={props.user.signInPassword}
            onChange={props.onUserPasswordChange}
            startAdornment={
              <InputAdornment position="start">
                <VpnKeyRounded />
              </InputAdornment>
            }
          />
          <FormHelperText id="name-error-text">
            {!props.user.signInPassword ? 'Password is required' : ''}
          </FormHelperText>
        </FormControl>
        <Button variant="contained"
                disabled={!props.user.signInEmail || !props.user.signInPassword}
                type="submit"
                color="primary">
          Se connecter
        </Button>
      </form>
    </div>
  );
}

export default SignIn;
