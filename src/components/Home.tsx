import * as React from 'react';

import './Home.css';

class Home extends React.Component {
  public render() {
    return (
      <div className="Home">
        <div className="WelcomeMessage">
          <div>Bienvenue au gîtes de La Moinerie</div>
        </div>
      </div>
    );
  }
}

export default Home;
