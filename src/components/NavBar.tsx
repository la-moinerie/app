import * as React from 'react';
import { Link } from 'react-router-dom';

import { IUser } from '../types';

import './NavBar.css';

export interface IProps {
  user: IUser;
}

function NavBar(props: IProps) {
  return (
    <div className="NavBar">
      <Link to="/">Le Mascaret de La Moinerie</Link>
      <Link to="/reservations">Réservations</Link>
      <Link to="/sign-in">Se connecter</Link>
      <span>{props.user.email ? props.user.email : ''}</span>
    </div>
  )
}

export default NavBar;