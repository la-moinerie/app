import { connectRouter, routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';

import rootReducer from './reducers';

export const initState = {
  reservations: [],
  user: {
    signInEmail: '',
    signInPassword: '',
  }
};

export const history = createBrowserHistory();

export const store = createStore(
  connectRouter(history)(rootReducer),
  initState,
  compose(
    applyMiddleware(
      thunk,
      routerMiddleware(history),
    ),
  ),
);