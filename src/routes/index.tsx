import * as React from 'react'
import { Route, Switch } from 'react-router'

import Home from '../components/Home';
import NavBar from '../containers/NavBar'
import SignIn from '../containers/SignIn';

const routes = (
  <React.Fragment>
    <NavBar />
    <Switch>
      <Route exact={true} path="/" component={Home} />
      <Route path="/sign-in" component={SignIn} />
    </Switch>
  </React.Fragment>
)

export default routes;